module.exports = ({ env }) => ({
  connection: {
    client: 'mysql',
    connection: {
      host: env('DATABASE_HOST', 'mysql-musiclima.alwaysdata.net'),
      port: env.int('DATABASE_PORT', 3306),
      database: env('DATABASE_NAME', 'musiclima_cafestock'),
      user: env('DATABASE_USERNAME', 'musiclima'),
      password: env('DATABASE_PASSWORD', 'Music2258'),
      ssl: env.bool('DATABASE_SSL', true),
    },
  },
});
