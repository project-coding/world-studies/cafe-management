'use strict';

/**
 * caafe service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::caafe.caafe');
