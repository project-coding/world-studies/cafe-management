'use strict';

/**
 * caafe controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::caafe.caafe');
