'use strict';

/**
 * caafe router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::caafe.caafe');
