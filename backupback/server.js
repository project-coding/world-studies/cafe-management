var express = require('express');
var cors = require('cors');
var app = express();
var mysql = require('mysql');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({ origin: true }));
app.use(cors());

//create database connection
const conn = mysql.createConnection({
    host: 'mysql-musiclima.alwaysdata.net',
    user: 'musiclima',
    password: 'Music2258',
    database: 'musiclima_cafestock'
});

//connect to database
conn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Connected...');
});


//Static folder
app.use('/', express.static('../dist'));

//Get all products
app.get('/basecafe', (req, res) => {
    let sql = "SELECT * FROM Base_Cafe";
    let query = conn.query(sql, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results }));
    });
});

//Get one products
app.get('/basecafe/:id', (req, res) => {
    const id = req.params.id; //product id
    let sql = "SELECT * FROM Base_Cafe WHERE id=?";
    let query = conn.query(sql, id, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results[0] }));
    });
});


//add new product
app.post('/basecafe', (req, res) => {
    let data = req.body; //{name:xxx, qty:xxx, price:xxx}
    let sql = "INSERT INTO Base_Cafe SET ?";
    let query = conn.query(sql, data, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results }));
    });
});

//update product
app.put('/basecafe/:id', (req, res) => {
    const id = req.params.id; //product id
    const pd = req.body;
    console.log(pd)

    let sql = "UPDATE Base_Cafe SET name=?, email=? WHERE id=?";
    let query = conn.query(sql, [pd.name, pd.email,id ], (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results }));
    });
});

//Delete product
app.delete('/basecafe/:id', (req, res) => {
    const id = req.params.id;
    let sql = "DELETE FROM Base_Cafe WHERE id=?";
    let query = conn.query(sql, id, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results }));
    });
});

//Server listening
app.listen(3000, () => {
    console.log('Server started on port 3000...');
});
